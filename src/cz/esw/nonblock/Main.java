package cz.esw.nonblock;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * @author Marek Cuchý
 */
public class Main {
	private static final long SEED = 0;

	private final static int MAX_THREADS = 200;
	private final static float ADD_PORTION = 1f;

	private final static int TOTAL_BENCHMARK_OPERATIONS = 30_000;
	private final static int REPEAT_BENCHMARK = 5;
	private static final int MAX_VALUE = 10000;
	private static final int MAX_LEVEL = 10;

	private static long test(int threads, IntSet set) throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(threads);

		List<SetWriter> writers = new ArrayList<>();
		for (int i=0; i< threads; i++) {
			writers.add(new SetWriter(TOTAL_BENCHMARK_OPERATIONS /threads, set, ADD_PORTION, MAX_VALUE, new Random(i)));
		}

		long t1 = System.nanoTime();

		List<Future<Stat>> futures = executor.invokeAll(writers);
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.HOURS);
		long t2 = System.nanoTime();

		List<Stat> stats = new ArrayList<>();
		for (Future<Stat> future : futures) {
			stats.add(future.get());
		}

		Stat total = new Stat();
		stats.forEach(total::add);
		System.out.println("TOTAL: " + total);


		System.out.println(threads+" thread(s) " + set.getClass().getSimpleName() + " TIME: " + (t2 - t1)/1_000_000 + " ms");

//		set.print();
//		set.validate();
		return t2-t1;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		for (int i =1; i < MAX_THREADS; i++) {
			for (int j = 0; j < REPEAT_BENCHMARK; j++) {
				long time1 = test(i, new NonblockLinkedListSet());
			}
		}
	}
}
