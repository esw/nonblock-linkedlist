package cz.esw.nonblock;

/**
 * @author Marek Cuchý
 */
public interface IntSet {

	public boolean contains(int value);
	public boolean add(int value);
	public boolean delete(int value);
	public void print();
	public void validate();
}
