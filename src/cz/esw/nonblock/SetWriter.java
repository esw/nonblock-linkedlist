package cz.esw.nonblock;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * @author Marek Cuchý
 */
public class SetWriter implements Callable<Stat> {

	private final static int MASK = 0xFFFFF00;
	private final static int MULTIPLIER = 256;

	private final IntSet set;

	private final int count;
	private final int add;
	private final Random rnd;
	private final int maxValue;

	private final Stat stat = new Stat();

	public SetWriter(int count, IntSet set, float offerPortion, int maxValue, Random rnd) {
		this.count = count;
		this.set = set;
		this.add = (int) ((1f - offerPortion) * MULTIPLIER);
		this.rnd = rnd;
		this.maxValue = maxValue;
	}

	@Override
	public Stat call() {
		long id = Thread.currentThread().getId();
		int a = 0;
		int b = 0;
		for (int i = 0; i < count; i++) {
			a += add;
			int q = a & MASK;
			if (b == q) {
				if(set.add(rnd.nextInt(maxValue))){
					stat.added();
				}else{
					stat.notAdded();
				}
			} else {
				if (set.delete(rnd.nextInt(maxValue))) {
					stat.deleted();
				}else{
					stat.notDeleted();
				}
				b = q;
			}
		}
		return stat;
	}
}
