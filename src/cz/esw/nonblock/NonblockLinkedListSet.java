package cz.esw.nonblock;

/**
 * @author Marek Cuchý
 */
public class NonblockLinkedListSet implements IntSet {

	//empty head and tail nodes
	private final Node head;
	private final Node tail;

	public NonblockLinkedListSet() {
		this.head = new Node();
		this.tail = new Node();
	}

	@Override
	public boolean contains(int value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean add(int value) {
		return false;
	}

	@Override
	public boolean delete(int value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void print() {
	}

	@Override
	public void validate() {
	}

	/**
	 * It finds the largest smaller element than {@code value} and the smallest larger or equal element than {@code value} and stores the results into the input arrays. The arrays workaround for C/C++ pointers.
	 *
	 * @param prevResult
	 * @param currResult
	 * @param value
	 */
	private void find(Node[] prevResult, Node[] currResult, int value) {
	}

	private static class Node {

	}
}
